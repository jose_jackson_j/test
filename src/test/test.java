package test;

import java.util.*;

public class test {

	public static void main(String[] args) {

		// Creating an empty Hashtable
		Hashtable<String, Movie> hash_table = new Hashtable<String, Movie>();

		Movie m = new Movie();
		m.setName("jose");
		m.setRating(1);

		// Inserting the values into table
		hash_table.put("Master", m);

		// Displaying the Hashtable
		System.out.println("Initial table is: " + hash_table);

		try {

			Movie x = hash_table.get("chellappa");
			// System.out.println(x.getRating());

		} catch (NullPointerException e) {
			// TODO: handle exception
			System.out.println(e);

		} catch (Exception e) {
			System.out.println(e);
		}

		// Getting the value of "Geeks"

		System.out.println("The Value is: " + hash_table.get("Master"));

		// Getting the value of "You"
		System.out.println("The Value is: " + hash_table.get("You"));
	}
}