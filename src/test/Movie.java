package test;

public class Movie {
	
	Movie(){
		
	}

	public Movie(String name, Integer rating) {
		super();
		this.name = name;
		this.rating = rating;
	}

	private String name;
	private Integer rating;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

}
